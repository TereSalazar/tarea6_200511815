FROM python:3.7.7-alpine3.10
RUN pip3 install flask
RUN pip3 install MySQL-connector-python
RUN mkdir -p  /flask/app
EXPOSE 80
CMD ["python3","/flask/app/servidor.py"]