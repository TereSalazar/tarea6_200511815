# TABLA DE CONTENIDO

* Información General
* Tecnología
* Instalación
* Video

## Información general

Realizar dos contenedores con Docker Compose en el cual uno sera un servidor web y el otro una base de datos. 

## Tecnología

* Docker

## Instalación

* Descargar o clonar proyecto
* Dirigirse en consola a la carpeta donde se descargó o clonó el proyecto
* Escribir el siguiente comando para ejecutar docker-compose up


## Video Demo

[enlace](https://youtu.be/X7oXnH7vq3s)