import mysql.connector
from flask import Flask, render_template

app = Flask(__name__)

def coneccion():
    mydb = mysql.connector.connect(
        host="dbEscuela",
        user="root",
        passwd="123456",
        database="escuela"
    )

    mycursor = mydb.cursor()

    mycursor.execute("SELECT * FROM estudiante")

    myresult = mycursor.fetchall()

    lst = []
    for x in myresult:
        lst.append(x)
    
    mydb.close()

    return lst

@app.route('/')
def index():
    datos = coneccion()
    
    return render_template("design.html", datos=datos)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)