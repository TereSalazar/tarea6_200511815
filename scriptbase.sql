CREATE DATABASE escuela;

USE escuela;

CREATE TABLE estudiante(
    id_estudiante int auto_increment, nombre varchar(55), apellido varchar(55), PRIMARY KEY(id_estudiante) 
);

INSERT INTO estudiante(nombre,apellido) VALUES ("Carlos","Lopez");

INSERT INTO estudiante(nombre,apellido) VALUES ("Paola","Salazar");

INSERT INTO estudiante(nombre,apellido) VALUES ("Teresa","Batz");

INSERT INTO estudiante(nombre,apellido) VALUES ("Alex","Ramirez");
